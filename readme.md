# Python scripts
Very small scripts illustrating Python programming

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `Python 3.6` or higher

## Usage
* python3 bmi.py
* python3 calories.py
* python3 pyramid.py

The user is prompted to input certain values.

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
