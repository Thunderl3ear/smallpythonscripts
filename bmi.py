# BMI Calculator
# Author: Thorbjørn Koch

if __name__ == "__main__":
    weight = float(input("Enter your weight in kg [65.4]: "))
    height = float(input("Enter your height in meters [1.64]: "))
    bmi = weight/(height**2)
    print("Your BMI is: "+"{0:.2f}".format(bmi))