# Print a pyramid of letters
# Author: Thorbjørn Koch

if __name__ == "__main__":
    while(True):
        n = int(input("Enter the pyramid size [24]: "))
        if(n>0 and n<27):
            break
        else:
            print("Invalid value. Accepted range [1,26]")
    
    letters = "".join([chr(97+i) for i in range(n)])
    print("".join([((n-1-i)*" "+letters[0:i+1]+letters[0:i][::-1]+(n-1-i)*" "+"\n"*(i<n-1)) for i in range(n)]))