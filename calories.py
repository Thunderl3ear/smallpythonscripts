# Calories Calculator
# Author: Thorbjørn Koch

if __name__ == "__main__":
    weight = float(input("Enter your weight in kg [65.4]: "))
    height = float(input("Enter your height in cm [164]: "))
    age = float(input("Enter your age in years [27]: "))

    bmr_male = 10*weight+6.25*height-5*age+5
    bmr_female = bmr_male-166
    gender = input("Are you male? [Y/n]: ")
    if(gender=="n"):
        bmr = bmr_female
    else:
        bmr = bmr_male
    print("Your BMR is: "+"{0:.2f}".format(bmr))